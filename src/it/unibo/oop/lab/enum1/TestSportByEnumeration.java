package it.unibo.oop.lab.enum1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import static it.unibo.oop.lab.enum1.Sport.*;
import it.unibo.oop.lab.socialnetwork.SocialNetworkUser;
import it.unibo.oop.lab.socialnetwork.SocialNetworkUserImpl;
import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        // TODO
         final SportSocialNetworkUserImpl<User> dwashington = new SportSocialNetworkUserImpl<>("Denzel", "Washington", "dwashington", 59);
         // TEST on DENZEL
         dwashington.addSport(BASKET);
         dwashington.addSport(SOCCER);
         dwashington.addSport(VOLLEY);
         dwashington.addSport(BASKET);
        System.out.println("Test aggiunta sport: "+ (dwashington.hasSport(BASKET) &&  dwashington.hasSport(SOCCER) && dwashington.hasSport(VOLLEY) && !dwashington.hasSport(F1)&& !dwashington.hasSport(MOTOGP)&& !dwashington.hasSport(BIKE)&& !dwashington.hasSport(TENNIS)));

      
    }

}
