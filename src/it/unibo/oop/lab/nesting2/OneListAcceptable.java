package it.unibo.oop.lab.nesting2;

import java.util.List;

public class OneListAcceptable<T> implements Acceptable<T> {

	private final List<T> list;
	
	
	
	public OneListAcceptable(List<T> list) {
		this.list = list;
	}



	public Acceptor<T> acceptor() {
		return new Acceptor<T>(){
			private int index=0;
			
			public void accept(T newElement) throws ElementNotAcceptedException {
				try {
					if (!(OneListAcceptable.this.list.get(this.index).equals(newElement))) {
						throw new ElementNotAcceptedException(newElement);
					} else {
						this.index++;
					}
				} catch (Exception e) {
					throw new ElementNotAcceptedException(newElement);
				}
			}

			@Override
			public void end() throws EndNotAcceptedException {
				if (this.index<OneListAcceptable.this.list.size()) {
					throw new EndNotAcceptedException();
				}
				
			}
			
		};
	}

}
