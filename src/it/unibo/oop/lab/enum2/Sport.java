/**
 * 
 */
package it.unibo.oop.lab.enum2;
import static it.unibo.oop.lab.enum2.Place.*;

/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */
public enum Sport {
	BASKET(5,"basket",INDOOR),
	VOLLEY(6,"pallavolo",INDOOR),
	TENNIS(2,"tennis",INDOOR),
	BIKE(1,"bike",OUTDOOR),
	F1(1,"formula 1",OUTDOOR),
	MOTOGP(1,"motogp",OUTDOOR);
	
    /*
     * TODO
     * 
     * Declare the following sports:
     * 
     * - basket
     * 
     * - volley
     * 
     * - tennis
     * 
     * - bike
     * 
     * - F1
     * 
     * - motogp
     */
	private final int members;
	private final String name;
	private final Place place;
    /*
     * TODO
     * 
     * [FIELDS]
     * 
     * Declare required fields
     */
	private Sport(int members, String name, Place place) {
		this.members = members;
		this.name = name;
		this.place = place;
	}
	
    /*
     * TODO
     * 
     * [CONSTRUCTOR]
     * 
     * Define a constructor like this:
     * 
     * - Sport(final Place place, final int noTeamMembers, final String actualName)
     */
	 public boolean isIndividualSport() {
		 if (this.members==1) {
			 return true;
		 } else {
			 return false;
		 }
	 }
	 
	 public boolean isIndoorSport() {
		 if (this.place==INDOOR) {
			 return true;
		 } else {
			 return false;
		 } 
	 }
	 
	 public Place getPlace() {
		 return this.place;
	 }
	 
	 public String toString() {
		 return (this.name+" is played in "+ this.members+" players, and in " +this.place);
	 }
    /*
     * TODO
     * 
     * [METHODS] To be defined
     * 
     * 
     * 1) public boolean isIndividualSport()
     * 
     * Must return true only if called on individual sports
     * 
     * 
     * 2) public boolean isIndoorSport()
     * 
     * Must return true in case the sport is practices indoor
     * 
     * 
     * 3) public Place getPlace()
     * 
     * Must return the place where this sport is practiced
     * 
     * 
     * 4) public String toString()
     * 
     * Returns the string representation of a sport
     */
}
